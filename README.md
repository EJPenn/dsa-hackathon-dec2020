# DSA Hackathon Dec2020
1. Download the starting code notebook
2. Install the requisite packages into your environment (incl. boto3)
3. Add in the AWS S3 credentials sent to you on POISE
4. Uncomment the dataset your team has chosen to use

Good luck and have fun!